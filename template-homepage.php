<?php
/**
 * Template Name: Homepage Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

    <?php get_template_part( 'templates/homepage', 'main-cta' ); ?>

    <?php

        $layout = get_post_meta( get_the_ID(), 'homepage-blocks', true );
        $layout = $layout['enabled'];

        if ( $layout ) : foreach ( $layout as $key => $value ) {

            switch ( $key ) {
                case 'cta_boxes' : get_template_part( 'templates/cta-boxes' );
                    break;

                case 'textfield' : get_template_part( 'templates/homepage', 'textfield' );
                    break;

                case 'sm_map' : get_template_part( 'templates/homepage', 'sm-map' );
                    break;

                case 'gf_grid' : get_template_part( 'templates/homepage', 'gf-grid' );
                    break;
            }
        } endif;
    ?>


<?php endwhile; ?>