<aside id="sidebar" role="complementary" class="col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9 hidden-xs">
    
    <div class="sidebar-background">

        <?php if ( hji_getVariable( 'header_phone_text' ) || hji_getVariable( 'header_phone' ) ) : ?>

            <div class="contact-phone">
                <span><?php echo hji_getVariable( 'header_phone_text' ); ?></span>
                <span class="number"><?php echo hji_getVariable( 'header_phone' ); ?></span>
            </div>

        <?php endif; ?>

        <nav class="nav_sidebar" role="navigation">

            <?php hji_theme_vert_nav(); ?>

            <div class="socialmedia-sidebar"><?php echo hji_social_media_links(); ?></div>

        </nav>

        <?php if ( is_active_sidebar( 'blvd-main-sidebarwidgets' ) ) : ?>

            <div id="primary" class="widget-area">

            <?php dynamic_sidebar( 'blvd-main-sidebarwidgets' ); ?>

            </div>

        <?php endif; ?>

    </div>

</aside>