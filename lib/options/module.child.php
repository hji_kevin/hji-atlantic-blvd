<?php
/**
 * Child Theme Options Module
 * Atlantic Child Theme
 */

if ( !function_exists( 'hji_boulevard_child_options' ) ) :
/**
 * 
 */
function hji_boulevard_child_options( $sections ) {
	$fields = array();

	$section = array(
		'icon'    => 'el-icon-plus-sign',
        'title'   => __('Child Theme Settings', 'hji-textdomain'),
        'heading' => __('', 'hji-textdomain'),
        'desc'    => __('', 'hji-textdomain'),
	);

	$fields[] = array(
		'title'    => __( 'Content Background Color', 'hji-textdomain' ),
		'desc'     => __( 'Choose the color that you want for the main areas and sidebar.', 'hji-textdomain' ),
		'id'       => 'content_color',
		'compiler' => true,
		'default'  => '#000000',
		'type'     => 'color',
	);

	$fields[] = array(
        'title'    => __( 'Content Transparency', 'hji-textdomain' ),
        'desc'     => __( 'Choose the transparency that you want for the main areas and sidebar.', 'hji-textdomain' ),
        'id'       => 'content_trans',
        'compiler' => true,
        'default'  => 70,
        'min'      => 1,
        'step'     => 1,
        'max'      => 100,
        'type'     => 'slider',
    );

	$fields[] = array(
		'id'    => 'child_help_2',
		'title' => __( 'Slideshow Height', 'hji-textdomain' ),
		'desc'  => __( 'By deafult the slideshow height on this theme is set to 95% of screen height. To override this setting so that the slideshow height theme option works as expected, please switch the option below to override.', 'hji-textdomain' ),
		'style' => 'warning',
		'type'  => 'info'
	);

	$fields[] = array(
		'id'       => 'slider_height_override',
		'title'    => __( 'Slideshow Height Override', 'hji-textdomain' ),
		'desc'     => __( '', 'hji-textdomain' ),
		'compiler' => true,
		'options'  => array(
			'0' => __( 'Default', 'hji-textdomain' ),
			'1' => __( 'Override', 'hji-textdomain' ),
		),
		'default'  => '0',
		'type'     => 'button_set',
	);

	$fields[] = array(
        'id'       => 'child_help_1',
        'title'    => __( 'Header Contact Info', 'hji-textdomain' ),
        'desc'     => __( 'The contact info in the header is designed for a contact phone number and call out. HTML can be used below.', 'hji-textdomain' ),
        'type'     => 'info'
    );

	$fields[] = array(
		'title'       => __( 'Text Above Phone', 'hji-textdomain' ),
		'desc'        => __( 'Enter the text above the phone number.', 'hji-textdomain' ),
		'subtitle'    => __( 'You can enter html in this area as well.', 'hji-textdomain' ),
		'id'          => 'header_phone_text',
		'placeholder' => 'Call Us Today...',
		'type'        => 'text',
    );

	$fields[] = array(
		'title'       => __( 'Phone Number', 'hji-textdomain' ),
		'desc'        => __( 'Enter the phone number you want to display in the header.', 'hji-textdomain' ),
		'subtitle'    => __( 'You can enter html in this area as well.', 'hji-textdomain' ),
		'id'          => 'header_phone',
		'placeholder' => '555-555-1234',
		'type'        => 'text',
    );


	$section['fields'] = $fields;

	$sections[] = $section;
	return $sections;

}
add_filter( 'redux/options/'. HJI_THEME_OPT_NAME .'/sections', 'hji_boulevard_child_options', 15 );
endif;



/**
 * Adjusts the content and sidebar transparency
 */
function hji_atlantic_blvd_content_trans( $variables ) {
	global $hji_theme_options;

	$content_color = $hji_theme_options['content_color'];

	if ( $content_color != '' ) {
		$content_bg =  '#' . str_replace( '#', '', HJI_Theme_Color::sanitize_hex( $hji_theme_options['content_color'] ) );
	} else {
		$content_bg = '#' . str_replace( '#', '', HJI_Theme_Color::sanitize_hex( $hji_theme_options['body_bg']['background-color'] ) );
	}

	$transparency = HJI_Theme_Color::get_rgba( $content_bg, $hji_theme_options['content_trans'] );

	$output = '$transparent-background: '. $transparency .';';

	$output .= '$content-color: '. $content_bg .';';

	$output .= '$slider-override: '. $hji_theme_options['slider_height_override'] .';';

	$variables .= $output;

	return $variables;
}
add_filter( 'hji_compiler_after_vars', 'hji_atlantic_blvd_content_trans', 99 );