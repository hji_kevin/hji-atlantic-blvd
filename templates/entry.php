<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="post-thumb">

    	<?php if ( has_action( 'hji_theme_featured_media' ) ) :

       		do_action( 'hji_theme_featured_media' );

       	else :

       		echo hji_theme_featured_image();

      endif; ?>

    </div>

    <div class="post-content">

    	<?php get_template_part( 'templates/entry-title' ); ?>

    	<?php echo apply_filters( 'hji_theme_do_the_excerpt', get_the_excerpt() ); ?>

    </div>

</article>