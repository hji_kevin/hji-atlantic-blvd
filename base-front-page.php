<!DOCTYPE html>

<html <?php language_attributes(); ?>>

    <?php get_header( hji_theme_template_base() ); ?>

    <body <?php body_class(); ?>>

        <!--[if lt IE 8]>
            <div class="alert alert-warning">
                <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'hji-textdomain'); ?>
            </div>
        <![endif]-->

        <div id="wrapper">

            <?php do_action( 'hji_theme_before_navbar' ); ?>
            
            <?php get_template_part( 'templates/header-navbar' ); ?>
            
            <?php do_action( 'hji_theme_after_navbar' ); ?>

            <div class="blvd-slideshow"></div>

            <section id="primary" class="container-fluid">
                
                <div class="row hidden-xs">

                    <div class="col-sm-4 col-md-3">

                        <a class="sidebar-brand" href="<?php echo home_url(); ?>/" title="<?php esc_attr_e( get_bloginfo( 'name' ), 'hji_themework' ); ?>" rel="home">
                            <?php hji_headerlogo(); ?>
                        </a>

                    </div>

                </div>

                <div class="row">

                    <div id="content" class="col-sm-8 col-sm-push-4 col-md-9 col-md-push-3" role="main">
                        <?php if ( is_page_template( 'template-homepage.php' ) ) : ?>

                            <?php include_once( 'template-homepage.php' ); ?>

                        <?php else : ?>

                            <?php do_action( 'hji_theme_before_content_col' ); ?>

                            <?php include hji_theme_template_path(); ?>

                            <?php do_action( 'hji_theme_after_content_col' ); ?>

                            <?php get_template_part( 'templates/cta-boxes' ); ?>

                        <?php endif; ?>

                    </div>
                    
                    <?php get_sidebar( hji_theme_template_base() ); ?>

                </div>
            
            </section>

            <section class="container">

                <?php if ( is_active_sidebar( 'blvd-homewidgets' ) ) : ?>

                    <div class="blvd-home-widgets row">
                        <?php dynamic_sidebar( 'blvd-homewidgets'); ?>
                    </div>

                <?php endif; ?>

            </section>

            <?php get_footer( hji_theme_template_base() ); ?>

        </div>

    </body>

</html>