<?php
/**
 * Child Theme Functions
 *
 */

if ( !function_exists('hji_theme_vert_nav') ) :
/**
 * Vertical Nav
 */
function hji_theme_vert_nav() {

    $defaults = array(
        'theme_location'  => 'main-menu',
        // 'container'       => 'div',
        // 'container_class' => '',
        // 'container_id'    => '',
        // 'menu_class'      => 'menu',
        // 'menu_id'         => '',
        // 'echo'            => true,
        // 'fallback_cb'     => false,
        // 'before'          => '',
        // 'after'           => '',
        // 'link_before'     => '',
        // 'link_after'      => '',
        'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
        'depth'           => 4,
        'walker'          => new Vertical_Nav_Walker
    );

    if (has_nav_menu('main-menu')) :
        return wp_nav_menu( $defaults );
    endif;

}
endif;


/**
 * Sets our SCSS constant for the improved build process
 *
 * @since 2.0.0
 * @since Blvd 2.7.0
 */
if ( !defined( 'HJI_BLVD_SCSS' ) ) {
    define( 'HJI_BLVD_SCSS', true );
}


/**
 * Makes the backstretch slideshow show on all pages
 *
 * @since 2.0.0
 */
function hji_atlantic_blvd_backstretch( $backstretch_display ) {
    $backstretch_display = true;

    return $backstretch_display;
}
add_filter( 'hji_theme_backstretch_display', 'hji_atlantic_blvd_backstretch' );


/**
 * Removes sidebar options since our sidebar should always show for this theme
 * First it removes the theme option panel because this function trumps the one that loads the panel.
 * Second if filters the sidebar conditional to always be true.
 */
function hji_theme_sidebar_options(  ) {
    return true;
}
add_filter( 'hji_theme_display_sidebar', 'hji_theme_sidebar_options' );


/**
 * Adds additional class to the header of the site
 *
 * @since 2.0.0
 */
function hji_atlantic_navbar_class( $class ) {
    $class .= ' visible-xs';

    return $class;
}
add_filter( 'hji_theme_navbar_class', 'hji_atlantic_navbar_class' );


/**
 * Alters the main menu vertical nav to prevent parent items from navigating to their url
 */
function hji_atlantic_vertical_nav_js() {
    $output = <<<ATLANTIC_JS
    <script type="text/javascript">

        jQuery('.nav_sidebar a.dropdown-toggle').on( 'click', function (e) {
            e.preventDefault();
        });

    </script>
ATLANTIC_JS;

    echo $output;
}
add_action( 'wp_footer', 'hji_atlantic_vertical_nav_js' );